//
//  BRNewsDetailsViewController.h
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRNewsDetailsViewController : UIViewController <UIWebViewDelegate>
@property (nonatomic, copy) NSString *tinyUrl;
@end
