//
//  BRNewsTableViewCell.h
//  WestpacNews
//
//  Created by Razvan Balazs on 25/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#define OFFSET                     10.

#import <UIKit/UIKit.h>
#import "NewsObject.h"

@interface BRNewsTableViewCell : UITableViewCell

@property (nonatomic, retain) NewsObject *newsObj;

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;

@end
