//
//  BRNewsFetcher.h
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BRNewsFetcher;

@protocol BRNewsFetcherDelegate <NSObject>

- (void)fetcherDidFinish:(BRNewsFetcher *)sender withData:(NSArray *)newsArray;
- (void)fetcherDidFailWithError:(NSError*)error;

@end

@interface BRNewsFetcher : NSObject
@property (nonatomic, assign) id <BRNewsFetcherDelegate> delegate;

- (void) fetchDataForUrl:(NSURL*)urlString;
@end
