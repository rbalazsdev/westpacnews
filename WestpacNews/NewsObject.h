//
//  NewsObject.h
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsObject : NSObject

@property (nonatomic,copy) NSString *webHref;
@property (nonatomic,strong) NSNumber *identifier;
@property (nonatomic,copy) NSString *headLine;
@property (nonatomic,copy) NSString *slugLine;
@property (nonatomic,strong) NSDate *dateLine;
@property (nonatomic,copy) NSString *tinyUrl;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *thumbnailImageHref;
@property (nonatomic,strong) UIImage *image;

- (id)init;

+ (id)newsObjectClassWithProperties:(NSDictionary *)properties;

@end






/*
webHref: "http://www.smh.com.au/entertainment/music/the-shortlist-gig-guide-2531-october-20131024-2w3du.html",
identifier: 4856610,
headLine: "The Shortlist gig guide, 25-31 October",
slugLine: "The Shortlist Gig Guide, October 25",
dateLine: "2013-10-25T03:00:00+11:00",
tinyUrl: "http://www.smh.com.au/entertainment/-2w3du.html",
type: "article",
thumbnailImageHref: "http://images.smh.com.au/2013/10/24/4858473/th-beyonce-320x214.jpg"
*/