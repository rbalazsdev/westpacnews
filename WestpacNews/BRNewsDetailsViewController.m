//
//  BRNewsDetailsViewController.m
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRNewsDetailsViewController.h"

@interface BRNewsDetailsViewController ()
@property(nonatomic, retain) UIWebView *webview;
@property(nonatomic, retain) UIActivityIndicatorView *loadingIndicator;
@end

@implementation BRNewsDetailsViewController
@synthesize tinyUrl = _tinyUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.webview = [[[UIWebView alloc] initWithFrame:self.view.frame] autorelease];
    self.webview.delegate = self;
    
    self.webview.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webview];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.tinyUrl]];
    [self.webview loadRequest:urlRequest];
    
    self.loadingIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    [self.view addSubview:self.loadingIndicator];
    self.loadingIndicator.center  = self.view.center;
    self.loadingIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleBottomMargin ;
    
    [self.loadingIndicator startAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.loadingIndicator stopAnimating];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.loadingIndicator stopAnimating];
}


- (void)dealloc
{
    self.webview.delegate = nil;
    self.tinyUrl = nil;
    self.webview = nil;
    self.loadingIndicator = nil;
    
    [super dealloc];
}

@end
