//
//  BRNewsTableViewCell.m
//  WestpacNews
//
//  Created by Razvan Balazs on 25/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRNewsTableViewCell.h"

@implementation BRNewsTableViewCell
@synthesize imageView = _imageView,titleLabel = _titleLabel,descriptionLabel = _descriptionLabel, newsObj = _newsObj;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _imageView.contentMode = UIViewContentModeScaleToFill;
        _imageView.backgroundColor = [UIColor blackColor];
        [self.contentView addSubview:_imageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:16.]];
        [self.contentView addSubview:_titleLabel];

        _descriptionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _descriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [_descriptionLabel setFont:[UIFont systemFontOfSize:16.]];
        _descriptionLabel.numberOfLines = 0;
        [_descriptionLabel setTextColor:[UIColor lightGrayColor]];
        [self.contentView addSubview:_descriptionLabel];
        
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(OFFSET,
                                      OFFSET,
                                      self.contentView.bounds.size.height*1.2,
                                      self.contentView.bounds.size.height - 2*OFFSET);
    self.imageView.hidden = !(self.imageView.image);
    
    NSInteger imageInset = 0;
    if (self.imageView.image) {
        imageInset = self.contentView.bounds.size.height*1.2 + OFFSET;
    }

    self.titleLabel.frame = CGRectMake(imageInset + OFFSET,
                                       OFFSET,
                                       self.contentView.bounds.size.width - imageInset - 2*OFFSET,
                                       20);

    self.descriptionLabel.frame = CGRectMake(imageInset + OFFSET,
                                             OFFSET +self.titleLabel.frame.size.height,
                                             self.contentView.bounds.size.width - imageInset - 2*OFFSET,
                                             self.contentView.bounds.size.height - self.titleLabel.frame.size.height - 2*OFFSET);
}

- (void)setNewsObj:(NewsObject *)newObj
{
    if (newObj != _newsObj) {
        [_newsObj release];
        _newsObj = [newObj retain];
    }
    self.imageView.image = _newsObj.image;
    self.titleLabel.text = _newsObj.headLine;
    self.descriptionLabel.text = _newsObj.slugLine;
}


- (void)dealloc
{
    self.newsObj = nil;
    self.imageView = nil;
    self.titleLabel = nil;
    self.descriptionLabel= nil;
    [super dealloc];
}

@end
