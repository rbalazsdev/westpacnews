//
//  BRNewsFetcher.m
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRNewsFetcher.h"
#import "NewsObject.h"

@implementation BRNewsFetcher
@synthesize delegate;

- (void)fetchDataForUrl:(NSURL *)urlString {
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlString];
    NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data,
                                               NSError *error){
                                                           if ([data length] > 0 && error == nil){
                                                               dispatch_sync(dispatch_get_main_queue(), ^{
                                                               [self.delegate fetcherDidFinish:self withData:[self serializeData:data]];
                                                               });
                                                           }
                                                           else if ([data length] == 0 && error == nil){
                                                               NSLog(@"Nothing downloaded.");
                                                               
                                                           }else if (error) {
                                                               NSLog(@"Download error: %@", [error description]);
                                                               [self.delegate fetcherDidFailWithError:error];
                                                           }
                                               }];
}

- (NSArray *)serializeData:(NSData *)responseData {
    NSError *error;
    if (!responseData) {
        return nil;
    }
    NSMutableArray *newsArray = [NSMutableArray arrayWithCapacity:0];
    id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData
                                                    options:kNilOptions
                                                      error:&error];
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        for (id dict in [[(NSDictionary *)jsonObject objectForKey:@"items"] allObjects] ) {
            NewsObject *newsObject = [NewsObject newsObjectClassWithProperties:dict];
            [newsArray addObject:newsObject];
        }
    }
    //sort the array
    [self sortArray:newsArray withDescriptorKey:@"dateLine" ascending:NO];
    return newsArray;
}

- (NSArray *) sortArray:(NSMutableArray*)array withDescriptorKey:(NSString *)descriptorKey ascending:(BOOL)ascending {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:descriptorKey ascending:ascending];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    return array;
}

- (void)dealloc {
    [super dealloc];
}
@end
