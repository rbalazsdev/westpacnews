//
//  BRNewsTableViewController.m
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "BRNewsTableViewController.h"
#import "BRNewsFetcher.h"
#import "NewsObject.h"
#import "BRNewsDetailsViewController.h"
#import "BRNewsTableViewCell.h"

@interface BRNewsTableViewController ()
{
    BRNewsFetcher *_newsFetcher;
}
@property (nonatomic,strong) NSArray *newsArray;
@end

@implementation BRNewsTableViewController
@synthesize newsArray = _newsArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _newsFetcher = [BRNewsFetcher new];
        _newsFetcher.delegate = self;
        
        _newsArray = [[NSArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_newsFetcher fetchDataForUrl:[NSURL URLWithString:INGESTURL]];
    [self.tableView setRowHeight:ROW_SIZE];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(refreshAction:)] autorelease];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    _newsFetcher.delegate = nil;
    [_newsFetcher release];
    _newsFetcher = nil;
    self.newsArray = nil;
    [super dealloc];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    BRNewsTableViewCell *cell = (BRNewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[BRNewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    // Configure the cell...
    NewsObject *obj = (NewsObject *)[self.newsArray objectAtIndex:indexPath.row];
    cell.newsObj = obj;
    
    if (obj.image) {
        [cell.imageView setImage:obj.image];
    }else if (obj.thumbnailImageHref != nil) {
        dispatch_queue_t queue = [dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) autorelease];
        dispatch_async(queue, ^{
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:obj.thumbnailImageHref]]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                obj.image = image;
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            });
        });
    }
    
    return cell;
}

#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BRNewsDetailsViewController *detailsVc = [[[BRNewsDetailsViewController alloc] initWithCoder:nil] autorelease];
    NewsObject *obj = (NewsObject *)[self.newsArray objectAtIndex:indexPath.row];
    detailsVc.tinyUrl = obj.tinyUrl;
    [self.navigationController pushViewController:detailsVc animated:YES];
}

#pragma mark - BRNewsFetcher Delegate methods

- (void)fetcherDidFinish:(BRNewsFetcher *)sender withData:(NSArray *)newsArray {
    self.newsArray = [NSArray arrayWithArray:newsArray];
   
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

- (void)fetcherDidFailWithError:(NSError *)error {
}

#pragma mark - RefreshAction

- (void)refreshAction:(id)sender {
    [_newsFetcher fetchDataForUrl:[NSURL URLWithString:INGESTURL]];

}
@end
