//
//  NewsObject.m
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//

#import "NewsObject.h"

@implementation NewsObject

+ (id)newsObjectClassWithProperties:(NSDictionary *)properties {
    return [[[self alloc] initWithProperties:properties] autorelease];
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        [self setValuesForKeysWithDictionary:properties];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (void)setDateLine:(NSDate *)dateLine {
    if ([dateLine isKindOfClass:[NSString class]]) {
        NSDateFormatter *formatter = [[NSDateFormatter new] autorelease];
        [formatter setDateStyle:NSDateFormatterFullStyle];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        [_dateLine release];
        _dateLine = [[NSDate alloc] init];
        _dateLine = [formatter dateFromString:(NSString*)dateLine];
    }
}


- (void) dealloc {
    self.webHref = nil;
    self.identifier = nil;
    self.headLine = nil;
    self.slugLine = nil;
    self.dateLine = nil;
    self.tinyUrl = nil;
    self.type = nil;
    self.thumbnailImageHref = nil;
    self.image = nil;
    [super dealloc];
}
@end
