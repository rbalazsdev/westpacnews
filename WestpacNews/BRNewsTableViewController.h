//
//  BRNewsTableViewController.h
//  WestpacNews
//
//  Created by Razvan Balazs on 24/10/2013.
//  Copyright (c) 2013 Razvan Balazs. All rights reserved.
//
#define INGESTURL @"http://mobilatr.mob.f2.com.au/services/views/9.json"
#define ROW_SIZE  ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? 100. : 150. )

#import <UIKit/UIKit.h>
#import "BRNewsFetcher.h"

@interface BRNewsTableViewController : UITableViewController <BRNewsFetcherDelegate>

@end
